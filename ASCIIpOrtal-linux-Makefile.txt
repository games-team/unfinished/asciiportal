# Makefile by kaw

# Requires PDCSDL, set PDCSDLDIR to point to a compiled version.

PDCSVER=3.4
PDCSDLDIR = ./PDCurses-$(PDCSVER)

PACKAGENAME = asciiportal-linux64
WINDOWSZIP = asciiportal.zip

GPPOPTS = -O2

all: asciiportal

clean:
	rm -f *.o
	rm -f asciiportal

fullclean: clean
	rm -rf $(PACKAGENAME)
	rm -f $(PACKAGENAME).tar.gz
	rm -rf $(PDCSDLDIR)

package: fullclean asciiportal
	strip asciiportal
	upx asciiportal
	mkdir $(PACKAGENAME)
	unzip -d $(PACKAGENAME) $(WINDOWSZIP)
	cp Makefile $(PACKAGENAME)/source
	rm $(PACKAGENAME)/*.exe
	rm $(PACKAGENAME)/*.bat
	rm $(PACKAGENAME)/*.dll
	cp asciiportal $(PACKAGENAME)/
	tar cvfz $(PACKAGENAME).tar.gz $(PACKAGENAME)

%.o: %.cpp $(PDCSDLDIR)
	g++ -I $(PDCSDLDIR) -c $(GPPOPTS) $<

asciiportal: al_input.o ap_draw.o ap_play.o ap_sound.o main.o menu.o $(PDCSDLDIR)/sdl1/libpdcurses.a
	g++ $^ -lSDL -lSDL_mixer -o $@

$(PDCSDLDIR)/sdl1/libpdcurses.a: $(PDCSDLDIR)
	cd `dirname $@` && make

$(PDCSDLDIR): PDCurses-$(PDCSVER).tar.gz
	tar xvfz $<

PDCurses-$(PDCSVER).tar.gz:
	wget http://sourceforge.net/projects/pdcurses/files/pdcurses/$(PDCSVER)/PDCurses-$(PDCSVER).tar.gz/download
	if [[ ! -f $@ ]]; then echo "Download of $@ failed, please provide file."; exit; fi


